using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using extensionlib;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using vd.web.Constants;
using vd.web.Entittes;
using vd.web.Extensions;
using vd.web.Services;

namespace vd.web.v1.Controllers
{
    [ApiController]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class SearchController : ControllerBase
    {
        #region Injected Members
        private readonly IDataRepository _dataRepository;
        private readonly ILoggerFactory _logger;
        private readonly IUrlHelper _urlHelper;
        private readonly IPropertyMappingService _propertyMappingService;
        private readonly ITypeHelperService _typeHelperService;
        private readonly IMapper _mapper;

        #endregion

        public SearchController(IDataRepository dataRepository,
                                ILoggerFactory logger,
                                IUrlHelper urlHelper,
                                IPropertyMappingService propertyMappingService,
                                ITypeHelperService typeHelperService,
                                IMapper mapper)
        {
            _dataRepository = dataRepository;
            _logger = logger;
            _urlHelper = urlHelper;
            _propertyMappingService = propertyMappingService;
            _typeHelperService = typeHelperService;
            _mapper = mapper;
        }

        [HttpGet("{filtertext}", Name=nameof(SearchBusiness))]
        public async Task<IActionResult> SearchBusiness(string filtertext)
        {
            if (filtertext.IsEmpty())
            {
                return BadRequest(ModelState);
            }

            var results = await _dataRepository.GetBusinessBySearch(filtertext);

            // resourceParams.Fields="adsh,name";
            
            var businessList = _mapper.Map<IEnumerable<SubDto>>(results.AsEnumerable());

            return Ok(businessList.ShapeData("adsh,name"));
        }
    }
}
