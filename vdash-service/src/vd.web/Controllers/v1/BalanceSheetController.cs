using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using extensionlib;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using vd.web.Constants;
using vd.web.Entittes;
using vd.web.Extensions;
using vd.web.Helpers;
using vd.web.Services;
using System.Web;


namespace vd.web.v1.Controllers
{
    [ApiController]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class BalanceSheetController : ControllerBase
    {
        #region Injected Members
        // private readonly IDataRepository _dataRepository;
        private readonly IBalanceSheetService _balanceSheetService;
        private readonly IUrlHelper _urlHelper;
        private readonly IPropertyMappingService _propertyMappingService;
        private readonly ITypeHelperService _typeHelperService;
        private readonly ILoggerFactory _logger;
        private readonly IMapper _mapper;
        #endregion

        #region Constructor
        public BalanceSheetController(IBalanceSheetService balanceSheetService,
                            IUrlHelper urlHelper,
                            IPropertyMappingService propertyMappingService,
                            ITypeHelperService typeHelperService,
                            ILoggerFactory logger,
                            IMapper mapper)
        {
            _logger = logger;
            // _dataRepository = dataRepository;
            _balanceSheetService=balanceSheetService;
            _urlHelper = urlHelper;
            _propertyMappingService = propertyMappingService;
            _typeHelperService = typeHelperService;
            _mapper=mapper;
        }
        #endregion


        
        #region Operations

        [HttpGet("{adsh}/{xbrl_version}/{date}",Name = nameof(GetBalanceSheet))]
        [HttpHead]
        public async Task<IActionResult> GetBalanceSheet(string adsh,
                                                        string xbrl_version,
                                                        string date,
                                                        [FromQuery]ResourceParams resourceParams,
                                                        [FromHeader(Name = "Accept")] string mediaType)
        {


            var result=await _balanceSheetService.GetBalanceSheet(HttpUtility.UrlDecode(adsh),HttpUtility.UrlDecode(xbrl_version),HttpUtility.UrlDecode(date));
            return Ok(result);

        //     // ResourceParams resourceParams = resourceParams;
        //     resourceParams.Adsh = adsh;
            
        //     var resultFromRepo = await  _dataRepository.GetPresentationData(adsh,ConstStatements.BS,version);

        //     var resultFrmNumbers = await _dataRepository.GetNumbers(adsh,version,date);

        //     var presentationData = Mapper.Map<IEnumerable<PreDto>>(resultFromRepo);

        //     if (mediaType == ConstMediaType.VD_API_HATEOAS)
        //     {
        //         // var paginationMetaData = new
        //         // {
        //         //     totalCount = resultFromRepo.TotalCount,
        //         //     pageSize = resultFromRepo.PageSize,
        //         //     currentPage = resultFromRepo.CurrentPage,
        //         //     totalPages = resultFromRepo.TotalPages
        //         // };

        //         Response.Headers.Add("X-Pagination", Newtonsoft.Json.JsonConvert.SerializeObject(paginationMetaData));

        //         var links = ResourceUriHelper.CreateLinksForHateos(nameof(GetBalanceSheet), resourceParams, resultFromRepo.HasNext, resultFromRepo.HasPrevious, _urlHelper);

        //         var shapedPresentatinData = presentationData.ShapeData(resourceParams.Fields).ToList();

        //         var shapedPresentatinDataLinks = shapedPresentatinData.ToList().Select(p =>
        //         {
        //             var presentationDataDictionary = p as IDictionary<string, object>;
        //             var presentationLinks = ResourceUriHelper.CreateLinks(nameof(GetBalanceSheet), (string)presentationDataDictionary["adsh"], null, resourceParams.Fields, _urlHelper);

        //             presentationDataDictionary.Add("links", presentationLinks);

        //             return presentationDataDictionary;
        //         });

        //         var linkedCollectionResource = new
        //         {
        //             data = shapedPresentatinDataLinks,
        //             links = links
        //         };

        //         return Ok(linkedCollectionResource);

        //     }
        //     else
        //     {
        //         var previousPageLink = resultFromRepo.HasPrevious ? ResourceUriHelper.CreateResourceUri(nameof(GetBalanceSheet), resourceParams, ResourceUriType.PreviousPage, _urlHelper) : null;
        //         var nextPageLink = resultFromRepo.HasNext ? ResourceUriHelper.CreateResourceUri(nameof(GetBalanceSheet), resourceParams, ResourceUriType.NextPage, _urlHelper) : null;

        //         var paginationMetaData = new
        //         {
        //             previousPageLink = previousPageLink,
        //             nextPageLink = nextPageLink,
        //             totalCount = resultFromRepo.TotalCount,
        //             pageSize = resultFromRepo.PageSize,
        //             currentPage = resultFromRepo.CurrentPage,
        //             totalPages = resultFromRepo.TotalPages
        //         };

        //         Response.Headers.Add("X-Pagination", Newtonsoft.Json.JsonConvert.SerializeObject(paginationMetaData));

        //         return Ok(presentationData.ShapeData(resourceParams.Fields));
        //     }
        }
        #endregion
    }
}