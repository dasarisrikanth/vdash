using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;
using vd.web.Services;
using vd.web.Entittes;
using AutoMapper;
using vd.web.Constants;
using vd.web.Helpers;
using vd.web.Extensions;
using extensionlib;

namespace vd.web.v1.Controllers
{

    [ApiController]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class BusinessController : ControllerBase
    {
        #region Injected Members
        private readonly IDataRepository _dataRepository;
        private readonly ILoggerFactory _logger;
        private readonly IUrlHelper _urlHelper;
        private readonly IPropertyMappingService _propertyMappingService;
        private readonly ITypeHelperService _typeHelperService;
        #endregion

        public BusinessController(IDataRepository dataRepository,
                            IUrlHelper urlHelper,
                            IPropertyMappingService propertyMappingService,
                            ITypeHelperService typeHelperService,
                            ILoggerFactory logger)
        {
            _logger = logger;
            _dataRepository = dataRepository;
            _propertyMappingService = propertyMappingService;
            _typeHelperService = typeHelperService;
            _urlHelper = urlHelper;
        }

        // [HttpGet("{adsh}", Name = "GetBusiness")]
        // [HttpHead]
        // [ActionName("GetBusiness")]
        // public async Task<IActionResult> GetBusinessAsync(string adsh, [FromHeader(Name = "Accept")] string mediaType)
        // {
        //     if (adsh == null)
        //     {
        //         throw new ArgumentNullException(nameof(adsh));
        //     }


        //     ResourceParams resourceParams = new ResourceParams();
        //     resourceParams.Adsh = adsh;

        //     var resultFromRepo = await _dataRepository.GetBusinessByAdsh(resourceParams.Adsh);

        //     if (resultFromRepo.IsNull()) return NotFound();

        //     var businessList = Mapper.Map<IEnumerable<SubDto>>(resultFromRepo);

        //     if (mediaType == ConstMediaType.VD_API_HATEOAS)
        //     {
        //         var paginationMetadata = new
        //         {
        //             totalCount = resultFromRepo.TotalCount,
        //             pageSize = resultFromRepo.PageSize,
        //             currentPage = resultFromRepo.CurrentPage,
        //             totalPages = resultFromRepo.TotalPages
        //         };

        //         Response.Headers.Add("X-Pagination", Newtonsoft.Json.JsonConvert.SerializeObject(paginationMetadata));

        //         var links = ResourceUriHelper.CreateLinksForHateos("GetBusiness", resourceParams, resultFromRepo.HasNext, resultFromRepo.HasPrevious, _urlHelper);

        //         var shappedBusiness = businessList.ShapeData(resourceParams.Fields).ToList();

        //         var shapedBusinessWithLinks = shappedBusiness.ToList().Select(business =>
        //         {

        //             var businessDictionary = business as IDictionary<string, object>;
        //             var businessLinks = ResourceUriHelper.CreateLinks("GetBusiness", (string)businessDictionary["adsh"], null, resourceParams.Fields, _urlHelper);
        //             businessLinks = businessLinks.Concat(ResourceUriHelper.CreateLinks("GetBalanceSheet", (string)businessDictionary["adsh"], null, null, _urlHelper));
        //             // businessLinks = businessLinks.Concat(ResourceUriHelper.CreateLinks("GetIncomeStatement", (string)businessDictionary["adsh"], null, null, _urlHelper));
        //             // businessLinks = businessLinks.Concat(ResourceUriHelper.CreateLinks("GetCashFlow", (string)businessDictionary["adsh"], null, null, _urlHelper));
        //             // businessLinks = businessLinks.Concat(ResourceUriHelper.CreateLinks("GetNumbers", (string)businessDictionary["adsh"], null, null, _urlHelper));
        //             businessDictionary.Add("links", businessLinks);

        //             return businessDictionary;
        //         });


        //         var linkedCollectionResource = new
        //         {
        //             value = shapedBusinessWithLinks,
        //             links = links
        //         };

        //         return Ok(linkedCollectionResource);

        //     }
        //     else
        //     {

        //         var previousPageLink = resultFromRepo.HasPrevious ? ResourceUriHelper.CreateResourceUri("GetBusiness", resourceParams, ResourceUriType.PreviousPage, _urlHelper) : null;
        //         var nextPageLink = resultFromRepo.HasNext ? ResourceUriHelper.CreateResourceUri("GetBusiness", resourceParams, ResourceUriType.NextPage, _urlHelper) : null;

        //         var paginationMetadata = new
        //         {
        //             previousPageLink = previousPageLink,
        //             nextPageLink = nextPageLink,
        //             totalCount = resultFromRepo.TotalCount,
        //             pageSize = resultFromRepo.PageSize,
        //             currentPage = resultFromRepo.CurrentPage,
        //             totalPages = resultFromRepo.TotalPages
        //         };

        //         Response.Headers.Add("X-Pagination", Newtonsoft.Json.JsonConvert.SerializeObject(paginationMetadata));

        //         return Ok(businessList.ShapeData(resourceParams.Fields));
        //     }
        // }
    }
}