namespace vd.web.Entittes {
    public class TagDto {

        public string tag1 { get; set; }

        public string version { get; set; }

        public bool? custom { get; set; }

        public bool? _abstract { get; set; }

        public string datatype { get; set; }

        public string iord { get; set; }

        public string crdr { get; set; }

        public string tlabel { get; set; }

        public string doc { get; set; }
    }
}