using System;

namespace vd.web.Entittes {
    public class NumDto {

        public string adsh { get; set; }

        public string tag { get; set; }

        public string version { get; set; }

        public string coreg { get; set; }

        public DateTime ddate { get; set; }

        public decimal qtrs { get; set; }

        public string uom { get; set; }

        public double? value { get; set; }

        public string footnote { get; set; }

    }
}