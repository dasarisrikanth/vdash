﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace vd.web.Entittes
{
    public class BalanceSheetDto
    {
        public string adsh { get; set; }
        public string version { get; set; }
        public string date { get; set; }      
        public IEnumerable<BasicTagValue> tagValues { get; set; }

    }
}
