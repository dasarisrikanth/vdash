﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace vd.web.Entittes
{
    public class BasicTagValue
    {
        public string Tag { get; set; }

        public string Value { get; set; }

        public string Label { get; set; }
    }
}
