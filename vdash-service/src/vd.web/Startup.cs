﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO.Compression;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json.Serialization;
using NLog.Extensions.Logging;
using vd.web.Services;
using vd.web.Helpers;
using vd.web.Extensions;
using vd.web.Logger;
using extensionlib;
using vd.web.ESLayer;
using AutoMapper;
using vd.web.MappingProfiles;
using vd.web.ESLayer.ESEntities;
using vd.web.Entittes;
using Swashbuckle.AspNetCore.Swagger;
using vd.web.Constants;
using Microsoft.AspNetCore.Mvc.Versioning;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.AspNetCore.Diagnostics;

namespace vd.web
{
    public class Startup
    {
        public const string AppS3BucketKey = "AppS3Bucket";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public static IConfiguration Configuration { get; private set; }

        // This method gets called by the runtime. Use this method to add services to the container
        
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc(setupAction=>{
                        setupAction.ReturnHttpNotAcceptable = true;
                        
                        var jsonInputFormatter = setupAction.InputFormatters
                        .OfType<JsonInputFormatter> ().FirstOrDefault ();

                        if (jsonInputFormatter.IsNotNull ()) {
                            jsonInputFormatter.SupportedMediaTypes.Add(ConstMediaType.VD_API_HATEOAS);
                        }

                        var jsonOutputFormatter = setupAction.OutputFormatters
                            .OfType<JsonOutputFormatter> ().FirstOrDefault ();

                        if (jsonOutputFormatter.IsNotNull ()) {
                            jsonOutputFormatter.SupportedMediaTypes.Add(ConstMediaType.VD_API_HATEOAS);
                        }

                    })
                    .AddJsonOptions(options=>{
                        options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver ();
                    })
                    .SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            
            services.AddResponseCompression(options=> {
                options.Providers.Add<GzipCompressionProvider> ();
            });

            services.Configure<GzipCompressionProviderOptions> (options => {
                options.Level = CompressionLevel.Fastest;
            });

            services.AddRouting(options=> options.LowercaseUrls=true);
            services.Configure<ESOptions>(Configuration.GetSection("ElasticSearch"));
            
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new SubMappingProfile());
                mc.AddProfile(new PreMappingProfile());
                mc.AddProfile(new NumMappingProfile());
            });

            IMapper mapper = mappingConfig.CreateMapper();
    
            services.AddSingleton(mapper)
                    .AddSingleton<ESClientProvider>()
                    .AddScoped<IDataRepository, DataRepository> ()
                    .AddScoped<IESRepository<sub>,ESRepository<sub>>()
                    .AddScoped<IESRepository<pre>,ESRepository<pre>>()
                    .AddScoped<IESRepository<num>, ESRepository<num>>()
                    .AddSingleton<IHttpContextAccessor, HttpContextAccessor>()
                    .AddTransient<IPropertyMappingService, PropertyMappingService> ()
                    .AddTransient<ITypeHelperService, TypeHelperService> ()
                    .AddTransient<IBalanceSheetService,BalanceSheetService>()
                    .AddSingleton<IActionContextAccessor, ActionContextAccessor>()
                    .AddScoped<IUrlHelper> (implementationFactory => {
                        var actionContext = implementationFactory.GetService<IActionContextAccessor> ().ActionContext;
                        var factory = implementationFactory.GetRequiredService<IUrlHelperFactory>();
                        return factory.GetUrlHelper(actionContext);
                    });
            
            services.AddApiVersioning(config=>{
                config.ReportApiVersions=true;
                config.AssumeDefaultVersionWhenUnspecified=true;
                config.DefaultApiVersion=new ApiVersion(1,0);
                config.ApiVersionReader=new HeaderApiVersionReader("api-version");
            });

            services.AddMvcCore().AddVersionedApiExplorer(o => o.GroupNameFormat = "'v'VVV");
            
            services.AddSwaggerGen(
                options=>
                {
                    var provider = services.BuildServiceProvider()
                                            .GetRequiredService<IApiVersionDescriptionProvider>();
                    
                    foreach(var description in provider.ApiVersionDescriptions)
                    {
                        options.SwaggerDoc(
                            description.GroupName,
                            new Info()
                            {
                                Title = $"VDash API",
                                Version = description.ApiVersion.ToString()
                            }
                        );
                    }
                }
            );

            // Add S3 to the ASP.NET Core dependency injection framework.
            services.AddAWSService<Amazon.S3.IAmazonS3>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline
        public void Configure(IApplicationBuilder app, IHostingEnvironment env,ILoggerFactory loggerFactory, IApiVersionDescriptionProvider provider)
        {
            loggerFactory.AddNLog();
            loggerFactory.AddConsole(Configuration.GetSection("Logging"))
                .AddDebug(LogLevel.Information)
                .AddESLogger(app.ApplicationServices, "log-", new FilterLoggerSettings
                {
                    {"*", LogLevel.Information}
                });

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                // app.UseBrowserLink();
            }
            else
            {
                app.UseHsts();
                app.UseExceptionHandler(appBuilder =>
                {
                    appBuilder.Run(async context =>
                    {
                        var exceptionHandlerFeature = context.Features.Get<IExceptionHandlerFeature>();
                        if (exceptionHandlerFeature != null)
                        {
                            var logger = loggerFactory.CreateLogger("Global exception logger");
                            logger.LogError(500,
                                exceptionHandlerFeature.Error,
                                exceptionHandlerFeature.Error.Message);
                        }

                        context.Response.StatusCode = 500;
                        await context.Response.WriteAsync("An unexpected fault happened. Try again later.");

                    });                      
                });
            }

            app.UseResponseCompression ();
            app.UseStaticFiles ();
            app.UseResponseWrapper();

            // app.UseHttpsRedirection(); // TODO: Figure this out later
            
            app.UseSwagger();
            app.UseSwaggerUI(c=>{
                foreach(var description in provider.ApiVersionDescriptions)
                {
                    c.SwaggerEndpoint(
                        $"/swagger/{description.GroupName}/swagger.json",
                        description.GroupName.ToUpperInvariant()
                    );
                }
            });
            
            app.UseMvc(route=>{
                route.MapRoute(
                    name:"default",
                    template:"{controller=Home}/{action=Index}/{id?}"
                );
            });

            app.UseStaticFiles(new StaticFileOptions
            {
                OnPrepareResponse = context =>
                {
                    context.Context.Response.Headers.Remove("Content-Length");
                }
            });

            // Mapper.Initialize (c =>
            //     c.AddProfiles (new [] {
            //         typeof(SubMappingProfile),
            //         typeof(PreMappingProfile),
            //         typeof(NumMappingProfile)
            //     })
            // );
        }
    }
}
