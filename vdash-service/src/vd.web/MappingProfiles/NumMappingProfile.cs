﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using extensionlib;
using System.Globalization;
using vd.web.Entittes;
using vd.web.ESLayer.ESEntities;

namespace vd.web.MappingProfiles
{
    public class NumMappingProfile:Profile
    {
        public NumMappingProfile()
        {
            CreateMap<num, NumDto>()
                    .ForMember(d=>d.ddate, opt=>opt.MapFrom(s=>DateTime.ParseExact(s.ddate,"yyyyMMdd",CultureInfo.InvariantCulture)));
                        
        }       
    }
}
