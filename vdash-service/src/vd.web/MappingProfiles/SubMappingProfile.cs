using System;
using System.Globalization;
using AutoMapper;
using extensionlib;
using vd.web.Entittes;
using vd.web.ESLayer.ESEntities;

namespace vd.web.MappingProfiles
{
    public class SubMappingProfile : Profile 
    {
        public SubMappingProfile () 
        {
            CreateMap<sub, SubDto>()
                    .ForMember(d => d.wksi, opt => opt.MapFrom(s=> s.wksi=="1" ? true : false))
                    .ForMember(d => d.cik,opt=>opt.MapFrom(s=>s.cik.ToDouble()))
                    .ForMember(d => d.sic,opt=>opt.MapFrom(s=>s.sic.ToDouble()))
                    .ForMember(d => d.ein,opt=>opt.MapFrom(s=>s.ein.ToDouble()))
                    .ForMember(d => d.fy,opt=>opt.MapFrom(s=>Convert.ToInt16(s.fy.ToInt())))
                    .ForMember(d => d.prevrpt, opt => opt.MapFrom(s=> s.prevrpt=="1" ? true : false))
                    .ForMember(d => d.detail, opt => opt.MapFrom(s=> s.detail=="1" ? true : false))
                    .ForMember(d => d.period,opt=>opt.MapFrom(s=>DateTime.ParseExact(s.period, "yyyyMMdd",CultureInfo.InvariantCulture)))
                    .ForMember(d => d.filed,opt=>opt.MapFrom(s=>DateTime.ParseExact(s.filed, "yyyyMMdd",CultureInfo.InvariantCulture)))
                    .ForMember(d => d.accepted,opt=>opt.MapFrom(s=>DateTime.ParseExact(s.accepted, "yyyy-MM-dd HH:mm:ss.f",CultureInfo.InvariantCulture)));
        }
    }

}