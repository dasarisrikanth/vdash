using AutoMapper;
using extensionlib;
using vd.web.Entittes;
using vd.web.ESLayer.ESEntities;

namespace vd.web.MappingProfiles
{
    public class PreMappingProfile:Profile
    {
        public PreMappingProfile()
        {
            CreateMap<pre, PreDto>()
                        .ForMember(d => d.report, opt => opt.MapFrom(s => s.report.ToDouble()))
                        .ForMember(d => d.line, opt => opt.MapFrom(s => s.line.ToDouble()))
                        .ForMember(d => d.inpth, opt => opt.MapFrom(s => s.inpth == "1" ? true : false));
        }
    }
}