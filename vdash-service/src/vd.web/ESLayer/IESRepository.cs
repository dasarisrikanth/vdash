using System.Collections.Generic;
using System.Threading.Tasks;
using vd.web.ESLayer.ESEntities;
namespace vd.web.ESLayer
{
    public interface IESRepository<T> where T:class
    {
        Task<BaseEntity<T>> GetAll();
        Task<BaseEntity<T>> Find(string query);

        Task<BaseEntity<T>> FindAll(string query);

        Task<BaseEntity<T>> FindWithMultipleFilters(IDictionary<string, string> filterCriteria);

        Task<T> FindById(string Id);
    }
}