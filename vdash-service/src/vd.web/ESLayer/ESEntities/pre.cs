namespace vd.web.ESLayer.ESEntities
{
    public class pre
    {
        public string adsh { get; set; }

        public string report { get; set; }

        public string line { get; set; }

        public string stmt { get; set; }

        public string inpth { get; set; }

        public string rfile { get; set; }

        public string tag { get; set; }

        public string version { get; set; }

        public string plabel { get; set; }

        public string negating { get; set; }
    }
}