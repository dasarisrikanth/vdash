using System;
using Nest;

namespace vd.web.ESLayer.ESEntities
{
    [Serializable]
    [Nest.ElasticsearchType(IdProperty="adsh")]
    public class sub 
    {
        [PropertyName("adsh")]
        public string adsh { get; set; }
        [PropertyName("cik")]
        public string cik { get; set; }
        [PropertyName("name")]
        public string name { get; set; }
        [PropertyName("sic")]
        public string sic { get; set; }
        [PropertyName("countryba")]
        public string countryba { get; set; }
        [PropertyName("stprba")]
        public string stprba { get; set; }
        [PropertyName("cityba")]
        public string cityba { get; set; }
        [PropertyName("zipba")]
        public string zipba { get; set; }
        [PropertyName("bas1")]
        public string bas1 { get; set; }
        [PropertyName("bas2")]
        public string bas2 { get; set; }
        [PropertyName("baph")]
        public string baph { get; set; }
        [PropertyName("countryma")]
        public string countryma { get; set; }
        [PropertyName("stprma")]
        public string stprma { get; set; }
        [PropertyName("cityma")]
        public string cityma { get; set; }
        [PropertyName("zipma")]
        public string zipma { get; set; }
        [PropertyName("mas1")]
        public string mas1 { get; set; }
        [PropertyName("mas2")]
        public string mas2 { get; set; }
        [PropertyName("countryinc")]
        public string countryinc { get; set; }
        [PropertyName("stprinc")]
        public string stprinc { get; set; }
        [PropertyName("ein")]
        public string ein { get; set; }
        [PropertyName("former")]
        public string former { get; set; }
        [PropertyName("changed")]
        public string changed { get; set; }
        [PropertyName("afs")]
        public string afs { get; set; }
        [PropertyName("wksi")]
        public string wksi { get; set; }
        [PropertyName("fye")]
        public string fye { get; set; }
        [PropertyName("form")]
        public string form { get; set; }
        [PropertyName("period")]
        public string period { get; set; }
        [PropertyName("fy")]
        public string fy { get; set; }
        [PropertyName("fp")]
        public string fp { get; set; }
        [PropertyName("filed")]
        public string filed { get; set; }
        [PropertyName("accepted")]
        public string accepted { get; set; }
        [PropertyName("prevrpt")]
        public string prevrpt { get; set; }
        [PropertyName("detail")]
        public string detail { get; set; }
        [PropertyName("instance")]
        public string instance { get; set; }
        [PropertyName("nciks")]
        public string nciks { get; set; }
        [PropertyName("aciks")]
        public string aciks { get; set; }
    }
}