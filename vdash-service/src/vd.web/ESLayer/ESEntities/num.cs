﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace vd.web.ESLayer.ESEntities
{
    public class num
    {
        public string adsh { get; set; }

        public string tag { get; set; }

        public string version { get; set; }

        public string coreg { get; set; }

        public string ddate { get; set; }

        public string qtrs { get; set; }

        public string uom { get; set; }

        public string value { get; set; }

        public string footnote { get; set; }
    }
}
