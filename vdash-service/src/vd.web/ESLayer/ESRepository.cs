using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Nest;
using vd.web.ESLayer.ESEntities;
using vd.web.Constants;
namespace vd.web.ESLayer
{
    public class ESRepository<T>:IESRepository<T> where T : class
    {
        private readonly ESClientProvider _esClientProvider;

        public ESRepository(ESClientProvider esClientProvider) 
        {
            _esClientProvider=esClientProvider;
        }

        public async Task<BaseEntity<T>> GetAll()
        {
            var response = await this._esClientProvider.Client.SearchAsync<T>(searchDescriptor=>searchDescriptor
                                        .Query(queryContainerDescriptor=>queryContainerDescriptor
                                            .MatchAll()));
            return MapResponseToSearchResult(response);
        }
        public async Task<T> FindById(string id) 
        {
            var resposne = await this._esClientProvider.Client.GetAsync<T>(id);
            return resposne.Source;
        }

        public async Task<BaseEntity<T>> Find(string query)
        {   
            /*
                {
                "from": 0,
                "size": 20,
                "query": {
                    "bool": {
                    "must": [
                        {
                        "query_string": {
                            "default_field": "name",
                            "query": "tech"
                        }
                        }
                    ]
                    }
                }
                }
            */

            var response = await this._esClientProvider.Client.SearchAsync<T>(searchDescriptor=>searchDescriptor
                                    .Query(queryContainerDescriptor=>queryContainerDescriptor
                                        .Bool(queryDescriptor=> queryDescriptor
                                            .Must(queryStringQuery => queryStringQuery
                                                .QueryString(queryString => queryString
                                                    .Query(query)))))
                                                        .From(ConstESParams.ESPageFrom)
                                                        .Size(ConstESParams.ESPageSize));
            Console.WriteLine(response.DebugInformation);
            return MapResponseToSearchResult(response, ConstESParams.ESPageFrom, ConstESParams.ESPageSize);
        }

        public async Task<BaseEntity<T>> FindAll(string query)
        {
            var response = await this._esClientProvider.Client.SearchAsync<T>(searchDescriptor=>searchDescriptor
                                    .Query(queryContainerDescriptor=>queryContainerDescriptor
                                        .Bool(queryDescriptor=> queryDescriptor
                                            .Must(queryStringQuery => queryStringQuery
                                                .QueryString(queryString => queryString
                                                    .Query(query))))));
            Console.WriteLine(response.DebugInformation);
            return MapResponseToSearchResult(response, ConstESParams.ESPageFrom, ConstESParams.ESPageFrom);
        }


        public async Task<BaseEntity<T>> FindWithMultipleFilters(IDictionary<string, string> filterCriteria)
        {
            var filters = new List<Func<QueryContainerDescriptor<T>, QueryContainer>>();

            foreach (KeyValuePair<string, string> fc in filterCriteria)
            {
                filters.Add(f => f.Match(t => t.Field(fc.Key).Query(fc.Value)));
            }

            var response = await this._esClientProvider.Client.SearchAsync<T>(searchDescriptor => searchDescriptor
                                    .Query(queryContainerDescriptor => queryContainerDescriptor
                                        .Bool(queryDescriptor => queryDescriptor
                                            .Must(filters)))
                                            .From(ConstESParams.ESPageFrom)
                                            .Size(ConstESParams.ESPageSize));

            Console.WriteLine(response.DebugInformation);
            return MapResponseToSearchResult(response, ConstESParams.ESPageFrom, ConstESParams.ESPageFrom);
        }



        private BaseEntity<T> MapResponseToSearchResult(ISearchResponse<T> response, int page, int pageSize)
        {
            return new BaseEntity<T>
            {
                IsValid = response.IsValid,
                ErrorMessage = response.ApiCall.OriginalException?.Message,
                Total = response.Total,
                ElapsedMilliseconds = response.Took,
                Page = page,
                PageSize = pageSize,
                Results = response?.Documents
            };
        }

        private BaseEntity<T> MapResponseToSearchResult(ISearchResponse<T> response)
        {
            return new BaseEntity<T>
            {
                IsValid = response.IsValid,
                ErrorMessage = response.ApiCall.OriginalException?.Message,
                Total = response.Total,
                ElapsedMilliseconds = response.Took,                
                Results = response?.Documents
            };
        }

        
    }
}   