using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Nest;
using extensionlib;
using vd.web.Entittes;
using vd.web.ESLayer.ESEntities;
using Elasticsearch.Net;

namespace vd.web.ESLayer
{
    public class ESClientProvider
    {

        public ESClientProvider(IOptions<ESOptions> settings) 
        {
            var connectionPool = new SingleNodeConnectionPool(new System.Uri(settings.Value.Uri));

            ConnectionSettings connectionSettings =
                new ConnectionSettings(connectionPool)
                                    .DefaultMappingFor<sub>(m => m.IndexName("sub")).EnableDebugMode().PrettyJson().RequestTimeout(TimeSpan.FromMinutes(2)) // .TypeName("doc"))
                                    .DefaultMappingFor<pre>(m => m.IndexName("pre")).EnableDebugMode().PrettyJson().RequestTimeout(TimeSpan.FromMinutes(2)) // .TypeName("doc"))
                                    .DefaultMappingFor<num>(m => m.IndexName("num")).EnableDebugMode().PrettyJson().RequestTimeout(TimeSpan.FromMinutes(2)); // .TypeName("doc"));
                    // .DefaultIndex(settings.Value.DefaultIndex)
                    // .DefaultTypeNameInferrer();
                    // .DefaultDisableIdInference();

            connectionSettings.EnableDebugMode();
            // connectionSettings.

            if (settings.Value.UserName.IsNotEmpty() && settings.Value.Password.IsNotEmpty())
            {
                connectionSettings.BasicAuthentication(settings.Value.UserName, settings.Value.Password);
            }

            this.Client = new ElasticClient(connectionSettings);

            // this.DefaultIndex = settings.Value.DefaultIndex;

            // EnsureIndexWithMapping<sub>(this.DefaultIndex); 
        }

        public ElasticClient Client {get;}
        public string DefaultIndex { get; private set; }

        public void EnsureIndexWithMapping<T>(string indexName = null, Func<PutMappingDescriptor<T>, PutMappingDescriptor<T>> customMapping = null) where T: class
        {
            if (String.IsNullOrEmpty(indexName)) indexName = this.DefaultIndex;

            // Map type T to that index
            this.Client.ConnectionSettings.DefaultIndices.Add(typeof(T), indexName);

            // Does the index exists?
            var indexExistsResponse = this.Client.IndexExists(new IndexExistsRequest(indexName));
            if (!indexExistsResponse.IsValid) throw new InvalidOperationException(indexExistsResponse.DebugInformation);

            // If exists, return
            if (indexExistsResponse.Exists) return;

            // Otherwise create the index and the type mapping
            var createIndexRes = this.Client.CreateIndex(indexName);
            if (!createIndexRes.IsValid) throw new InvalidOperationException(createIndexRes.DebugInformation);

            var res = this.Client.Map<T>(m =>
            {
                m.AutoMap().Index(indexName);
                if (customMapping != null) m = customMapping(m);
                return m;
            });
            
            if (!res.IsValid) throw new InvalidOperationException(res.DebugInformation);
        }

        
    }
}