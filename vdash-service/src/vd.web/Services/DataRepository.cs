using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using vd.web.Entittes;
using vd.web.Extensions;
using vd.web.Helpers;
using extensionlib;
using vd.web.ESLayer;
using AutoMapper;
using vd.web.ESLayer.ESEntities;
using vd.web.Constants;

namespace vd.web.Services 
{
    public class DataRepository : IDataRepository 
    {

        private readonly IESRepository<sub> _submissionRespository;
        private readonly IESRepository<pre> _preRepository;
        private readonly IESRepository<num> _numRepository;
        private readonly IMapper _mapper;
        // private IPropertyMappingService _propertyMappingService;

        public DataRepository (IESRepository<sub> submissionRespository,
                                IESRepository<pre> preRespository,
                                IESRepository<num> numRespository,
                                IMapper mapper)
                                // IPropertyMappingService propertyMappingService) 
        {
            _submissionRespository = submissionRespository;
            _preRepository = preRespository;
            _numRepository = numRespository;
            _mapper = mapper;
            // _propertyMappingService = propertyMappingService;
        }

        public async Task<bool> IsBusinessExists(string adsh)
        {
            if(adsh.IsEmpty()) {
                return false;
            }
            var result = await _submissionRespository.FindById(adsh); 
            return result.IsNotNull();
        }

        public async Task<IEnumerable<SubDto>> GetBusinessBySearch (string  searchString) 
        {
            if (searchString.IsEmpty ()) {
                return null;
            }
            
            var searchQueryObj = await _submissionRespository.Find(searchString.Trim().ToLowerInvariant());

            if(searchQueryObj.IsNotNull() && searchQueryObj.Results.IsNotNull()) 
            {
                var collectionBeforePaging = searchQueryObj.Results;
                return _mapper.Map<IEnumerable<SubDto>>(collectionBeforePaging);
            }
            else 
            {
                return null;
            }
        }

        public async Task<SubDto> GetBusinessByAdsh (string adsh) 
        {
            if(adsh.IsEmpty()) {
                return null;
            }
            var result = await _submissionRespository.FindById(adsh); 
            return _mapper.Map<SubDto>(result);
        }

        

        public async Task<IEnumerable<PreDto>> GetPresentationData(string adsh, ConstStatements statement, string version)
        {
            if(adsh.IsEmpty() || version.IsEmpty()) {
                return null;
            }

            IDictionary<string, string> filters = new Dictionary<string, string>
            {
                {"adsh.keyword",adsh },
                {"stmt.keyword",statement.ToString() },
                {"version.keyword",version}
            };

            var adshQuery = await _preRepository.FindWithMultipleFilters(filters);

            if(adshQuery.IsNotNull())
            {
                var collectionBeforePaging = adshQuery.Results;
                return _mapper.Map<IEnumerable<PreDto>>(collectionBeforePaging); 
            }
            else {
                return null;
            }
        }

        public async Task<IEnumerable<NumDto>> GetNumbers(string adsh, string version, string ddate)
        {
            if(adsh.IsEmpty() || version.IsEmpty() || ddate.IsEmpty())
            {
                return null;
            }

            IDictionary<string, string> filters = new Dictionary<string, string>
            {
                {"adsh.keyword",adsh },
                {"ddate.keyword",ddate},
                {"version.keyword",version }
            };

            var query = await _numRepository.FindWithMultipleFilters(filters);

            if(query.IsNotNull())
            {
                var collectionBeforePaging = query.Results;
                return _mapper.Map<IEnumerable<NumDto>>(collectionBeforePaging); //TODO : revist this again
            } else
            {
                return null;
            }

        }

        // public bool IsPresentationDataExist(string adsh)
        // {

        // }
    }
}