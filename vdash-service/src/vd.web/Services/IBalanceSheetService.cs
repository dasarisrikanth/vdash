﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using vd.web.Entittes;

namespace vd.web.Services
{
    public interface IBalanceSheetService
    {
        Task<BalanceSheetDto> GetBalanceSheet(string adsh, string version, string ddate);
    }
}
