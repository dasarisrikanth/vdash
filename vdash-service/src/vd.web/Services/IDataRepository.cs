using System.Collections.Generic;
using System.Threading.Tasks;
using vd.web.Constants;
using vd.web.Entittes;
using vd.web.Helpers;

namespace vd.web.Services 
{
    public interface IDataRepository 
    {
        #region Submissions 
        Task<IEnumerable<SubDto>> GetBusinessBySearch (string  searchString);

        Task<SubDto> GetBusinessByAdsh (string _adsh);

        Task<bool> IsBusinessExists(string adsh);
        #endregion

        #region Presenation Data 
        Task<IEnumerable<PreDto>> GetPresentationData(string adsh, ConstStatements statement, string version);

        // bool IsPresentationDataExist(string adsh);
        #endregion

        // #region Numbers 

        Task<IEnumerable<NumDto>> GetNumbers(string adsh,  string version, string ddate);

        // bool IsNumbersExist(string adsh);
        // #endregion        

    }
}