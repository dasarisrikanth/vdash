﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using vd.web.Constants;
using vd.web.Entittes;
using extensionlib;
namespace vd.web.Services
{
    public class BalanceSheetService : IBalanceSheetService
    {
        private readonly IDataRepository _dataRepository;
        public BalanceSheetService(IDataRepository dataRepository)
        {
            _dataRepository=dataRepository;
        }
        public async Task<BalanceSheetDto> GetBalanceSheet(string adsh, string version, string ddate)
        {
            if(adsh.IsEmpty() || version.IsEmpty() || ddate.IsEmpty()) 
            {
                return null;
            }

            BalanceSheetDto finalResult=new BalanceSheetDto();
            
            var business = await _dataRepository.GetBusinessByAdsh(adsh);

            var presentationData =await  _dataRepository.GetPresentationData(adsh,ConstStatements.BS,version);

            var numbers = await _dataRepository.GetNumbers(adsh,version,ddate);

            finalResult.adsh=adsh;
            finalResult.version=version;
            finalResult.date=ddate;
            var tagValues = new List<BasicTagValue>();
            presentationData.ForEach(x=>{
                var tag = new BasicTagValue(); 
                tag.Label=x.plabel;
                tag.Tag=x.tag;
                var _value=numbers.IfNotNull(t=>t).Where(y=>y.tag==x.tag).IfNotNull(z=>z.FirstOrDefault());
                tag.Value=_value!=null?_value.value.ToString():"";
                tagValues.Add(tag);
            });
            finalResult.tagValues=tagValues;

            return finalResult;
        }        
    }
}
