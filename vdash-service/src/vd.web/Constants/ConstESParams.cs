namespace vd.web.Constants
{
    public static class ConstESParams
    {
        public const int ESPageFrom = 0;
        public const int ESPageSize = 10000;
    }
}