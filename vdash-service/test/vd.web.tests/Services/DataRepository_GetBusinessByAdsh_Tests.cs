using System;
using System.Threading.Tasks;
using AutoFixture.Xunit2;
using AutoMapper;
using Moq;
using vd.web.Entittes;
using vd.web.ESLayer;
using vd.web.ESLayer.ESEntities;
using vd.web.MappingProfiles;
using vd.web.Services;
using vd.web.tests.Helpers;
using Xunit;

namespace vd.web.tests.Services
{
    public class DataRepository_GetBusinessByAdsh_Tests
    {
        private readonly IMapper mapper;
        public DataRepository_GetBusinessByAdsh_Tests()
        {
            var config = new MapperConfiguration(cfg=>{
                cfg.AddProfile(new SubMappingProfile());
            });

            mapper = config.CreateMapper();
        }

        [Theory, GenerateDefaultTestData]
        public async Task GetBusinessByAdsh_GivenValidInput_ExecuteWithOutException(string adsh,
                                                                                        IESRepository<sub> fakeSubmissionRespository,
                                                                                        IESRepository<pre> fakePreRepository,
                                                                                        IESRepository<num> fakeNumRepository)
        {
            // Arrange
            IDataRepository dataRepository = new DataRepository(fakeSubmissionRespository,fakePreRepository,fakeNumRepository, this.mapper);

            // Act
            Exception shouldBeNull = await Record.ExceptionAsync(()=>dataRepository.GetBusinessByAdsh(adsh));

            // Assert
            Assert.Null(shouldBeNull);
        }

        [Theory, GenerateDefaultTestData]
        public async Task GetBusinessByAdsh_GivenNullInput_ExecutesReturnNull(IESRepository<sub> fakeSubmissionRespository,
                                                                                                        IESRepository<pre> fakePreRepository,
                                                                                                        IESRepository<num> fakeNumRepository)
        { 
            // Arrange
            IDataRepository dataRepository = new DataRepository(fakeSubmissionRespository,fakePreRepository,fakeNumRepository, this.mapper);

            // Act
            var sut = await dataRepository.GetBusinessByAdsh(null);

            // Assert
            Assert.Null(sut);
        }


        [Theory, GenerateDefaultTestData]
        public async Task GetBusinessByAdsh_GivenEmptyString_ExecutesReturnNull(IESRepository<sub> fakeSubmissionRespository,
                                                                                                        IESRepository<pre> fakePreRepository,
                                                                                                        IESRepository<num> fakeNumRepository)
        { 
            // Arrange
            IDataRepository dataRepository = new DataRepository(fakeSubmissionRespository,fakePreRepository,fakeNumRepository, this.mapper);

            // Act
            var actual = await dataRepository.GetBusinessByAdsh("");

            // Assert
            Assert.Null(actual);
        }

        [Theory, GenerateDefaultTestData]
        public async Task GetBusinessByAdsh_GivenValidInput_ExecutesReturnValidObject(string adsh,[Frozen] Mock<IESRepository<sub>> mockESSubRepo,
                                                                                                        IESRepository<pre> fakePreRepository,
                                                                                                        IESRepository<num> fakeNumRepository,
                                                                                                        sub sampleSub)
        { 
            // Arrange
            
            mockESSubRepo.Setup(_=>_.FindById(It.IsAny<string>())).ReturnsAsync(sampleSub);
            IDataRepository dataRepository = new DataRepository(mockESSubRepo.Object,fakePreRepository,fakeNumRepository,this.mapper);

            // Act
            var sut = await dataRepository.GetBusinessByAdsh(adsh);

            // Assert
            Assert.NotNull(sut);
            Assert.IsType<SubDto>(sut);
        }
    }
}