using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoFixture.Xunit2;
using AutoMapper;
using Moq;
using vd.web.Constants;
using vd.web.Entittes;
using vd.web.ESLayer.ESEntities;
using vd.web.MappingProfiles;
using vd.web.Services;
using vd.web.tests.Helpers;
using Xunit;

namespace vd.web.tests.Services
{
    public class BalanceSheetService_GetBalanceSheet_Tests
    {

        private readonly IMapper mapper;

        public BalanceSheetService_GetBalanceSheet_Tests()
        {
            var config = new MapperConfiguration(cfg=>{
                cfg.AddProfile(new SubMappingProfile());
                cfg.AddProfile(new NumMappingProfile());
                cfg.AddProfile(new PreMappingProfile());
            });
        }
        
        [Theory, GenerateDefaultTestData]
        public async Task GetBalanceSheet_GivenValidInput_ExecuteWithoutException(string adsh, string version,string ddate,IDataRepository dataRepo)
        {
            // Arrange 
            IBalanceSheetService balanceSheet = new BalanceSheetService(dataRepo);
            // Act
            Exception shouldbeNull = await Record.ExceptionAsync(()=>balanceSheet.GetBalanceSheet(adsh,version,ddate));

            // Assert
            Assert.Null(shouldbeNull);
        }

        [Theory, GenerateDefaultTestData]
        public async Task GetBalanceSheet_GivenNullInput_ExecutesReturnNull(string adsh, string version,string ddate,IDataRepository dataRepo) 
        {
            // Arrange
            IBalanceSheetService balanceSheet = new BalanceSheetService(dataRepo);

            // ACT
            var sut = await balanceSheet.GetBalanceSheet(null,null,null);

            // Assert
            Assert.Null(sut);
        }

        [Theory, GenerateDefaultTestData]
        public async Task GetBalanceSheet_GivenEmptyString_ExecutesReturnNull(string adsh, string version,string ddate,IDataRepository dataRepo) 
        {
            // Arrange
            IBalanceSheetService balanceSheet = new BalanceSheetService(dataRepo);

            // ACT
            var sut = await balanceSheet.GetBalanceSheet("","","");

            // Assert
            Assert.Null(sut);
        }

        [Theory, GenerateDefaultTestData]
        public async Task GetBalanceSheet_GivenValidInput_ExecutesReturnNull(string adsh, 
                                                                                string version,
                                                                                string ddate,
                                                                                IDataRepository dataRepo, 
                                                                                [Frozen] Mock<IDataRepository> mockDataRepository ,
                                                                                SubDto sampleSub,
                                                                                IEnumerable<PreDto> samplePre,
                                                                                IEnumerable<NumDto> sampleNum,
                                                                                BalanceSheetDto sampleBalanceSheet) 
        {
            // Arrange
            mockDataRepository.Setup(x=>x.GetBusinessByAdsh(It.IsAny<string>())).ReturnsAsync(sampleSub);
            mockDataRepository.Setup(x=>x.GetPresentationData(It.IsAny<string>(),It.IsAny<ConstStatements>(),It.IsAny<string>())).ReturnsAsync(samplePre);
            mockDataRepository.Setup(x=>x.GetNumbers(It.IsAny<string>(),It.IsAny<string>(),It.IsAny<string>())).ReturnsAsync(sampleNum);
            IBalanceSheetService balanceSheet = new BalanceSheetService(dataRepo);

            // ACT
            var sut = await balanceSheet.GetBalanceSheet(adsh,version,ddate);

            // Assert
            Assert.NotNull(sut);

        }
    }
}