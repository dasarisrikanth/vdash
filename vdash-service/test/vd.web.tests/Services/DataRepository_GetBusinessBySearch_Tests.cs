
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoFixture.Xunit2;
using AutoMapper;
using Moq;
using vd.web.Entittes;
using vd.web.ESLayer;
using vd.web.ESLayer.ESEntities;
using vd.web.MappingProfiles;
using vd.web.Services;
using vd.web.tests.Helpers;
using Xunit;

namespace vd.web.tests.Services
{
    
    public class DataRepository_GetBusinessBySearch_Tests
    {
        private readonly IMapper mapper;
        public DataRepository_GetBusinessBySearch_Tests()
        {
            var config = new MapperConfiguration(cfg=>{
                cfg.AddProfile(new SubMappingProfile());
            });

            mapper = config.CreateMapper();
        }

        [Theory, GenerateDefaultTestData]
        public async Task GetBusinessBySearch_GivenValidInput_ExecutesWithOutException(string searchText, 
                                                                                                        IESRepository<sub> fakeSubmissionRespository,
                                                                                                        IESRepository<pre> fakePreRepository,
                                                                                                        IESRepository<num> fakeNumRepository)
        {
            // Arrange
            IDataRepository dataRepository = new DataRepository(fakeSubmissionRespository,fakePreRepository,fakeNumRepository, this.mapper);

            // Act
            Exception shouldBeNull = await Record.ExceptionAsync(()=>dataRepository.GetBusinessBySearch(searchText));

            // Assert
            Assert.Null(shouldBeNull);
        }


        [Theory, GenerateDefaultTestData]
        public async Task GetBusinessBySearch_GivenNullInput_ExecutesReturnNull(IESRepository<sub> fakeSubmissionRespository,
                                                                                                        IESRepository<pre> fakePreRepository,
                                                                                                        IESRepository<num> fakeNumRepository)
        { 
            // Arrange
            IDataRepository dataRepository = new DataRepository(fakeSubmissionRespository,fakePreRepository,fakeNumRepository, this.mapper);

            // Act
            var actual = await dataRepository.GetBusinessBySearch(null);

            // Assert
            Assert.Null(actual);
        }

        [Theory, GenerateDefaultTestData]
        public async Task GetBusinessBySearch_GivenEmptyString_ExecutesReturnNull(IESRepository<sub> fakeSubmissionRespository,
                                                                                                        IESRepository<pre> fakePreRepository,
                                                                                                        IESRepository<num> fakeNumRepository)
        { 
            // Arrange
            IDataRepository dataRepository = new DataRepository(fakeSubmissionRespository,fakePreRepository,fakeNumRepository, this.mapper);

            // Act
            var actual = await dataRepository.GetBusinessBySearch("");

            // Assert
            Assert.Null(actual);
        }

        [Theory, GenerateDefaultTestData]
        public async Task GetBusinessBySearch_GivenValidInput_ExecutesReurnValidObject(string searchText,[Frozen] Mock<IESRepository<sub>> mockESSubRepo,
                                                                                                        IESRepository<pre> fakePreRepository,
                                                                                                        IESRepository<num> fakeNumRepository,
                                                                                                        BaseEntity<sub> sampleSub)
        { 
            // Arrange
            
            mockESSubRepo.Setup(_=>_.Find(It.IsAny<string>())).ReturnsAsync(sampleSub);
            IDataRepository dataRepository = new DataRepository(mockESSubRepo.Object,fakePreRepository,fakeNumRepository,this.mapper);

            // Act
            var sut = await dataRepository.GetBusinessBySearch(searchText);

            // Assert
            Assert.NotNull(sut);
            Assert.Equal(sut.ToList().Count(),sampleSub.Results.ToList().Count());
            Assert.IsType<SubDto>(sut.ToList().First());
        }

    }
}