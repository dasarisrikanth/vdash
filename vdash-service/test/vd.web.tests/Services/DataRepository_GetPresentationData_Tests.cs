using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoFixture.Xunit2;
using AutoMapper;
using Moq;
using vd.web.Constants;
using vd.web.Entittes;
using vd.web.ESLayer;
using vd.web.ESLayer.ESEntities;
using vd.web.MappingProfiles;
using vd.web.Services;
using vd.web.tests.Helpers;
using Xunit;


namespace vd.web.tests.Services
{
    public class DataRepository_GetPresentationData_Tests
    {
        private readonly IMapper mapper;
        public DataRepository_GetPresentationData_Tests()
        {
            var config = new MapperConfiguration(cfg=>{
                cfg.AddProfile(new PreMappingProfile());
            });

            mapper = config.CreateMapper();
        }

        [Theory, GenerateDefaultTestData]
        public async Task GetPresentationData_GivenValidInput_ExecuteWithOutException(string adsh,
                                                                                        string version,
                                                                                        IESRepository<sub> fakeSubmissionRespository,
                                                                                        IESRepository<pre> fakePreRepository,
                                                                                        IESRepository<num> fakeNumRepository)
        {
            // Arrange
            IDataRepository dataRepository = new DataRepository(fakeSubmissionRespository,fakePreRepository,fakeNumRepository, this.mapper);

            // Act
            Exception shouldBeNull = await Record.ExceptionAsync(()=>dataRepository.GetPresentationData(adsh,ConstStatements.BS,version));

            // Assert
            Assert.Null(shouldBeNull);
        }

        [Theory, GenerateDefaultTestData]
        public async Task GetPresentationData_GivenNullInput_ExecutesReturnNull_Sceanrio1(IESRepository<sub> fakeSubmissionRespository,
                                                                                                        IESRepository<pre> fakePreRepository,
                                                                                                        IESRepository<num> fakeNumRepository)
        { 
            // Arrange
            IDataRepository dataRepository = new DataRepository(fakeSubmissionRespository,fakePreRepository,fakeNumRepository, this.mapper);

            // Act
            var sut = await dataRepository.GetPresentationData(null,ConstStatements.BS,null);

            // Assert
            Assert.Null(sut);
        }

        [Theory, GenerateDefaultTestData]
        public async Task GetPresentationData_GivenNullInput_ExecutesReturnNull_Sceanrio2(IESRepository<sub> fakeSubmissionRespository,
                                                                                                        IESRepository<pre> fakePreRepository,
                                                                                                        IESRepository<num> fakeNumRepository)
        { 
            // Arrange
            IDataRepository dataRepository = new DataRepository(fakeSubmissionRespository,fakePreRepository,fakeNumRepository, this.mapper);

            // Act
            var sut = await dataRepository.GetPresentationData("sds",ConstStatements.BS,null);

            // Assert
            Assert.Null(sut);
        }

        [Theory, GenerateDefaultTestData]
        public async Task GetPresentationData_GivenNullInput_ExecutesReturnNull_Sceanrio3(IESRepository<sub> fakeSubmissionRespository,
                                                                                                        IESRepository<pre> fakePreRepository,
                                                                                                        IESRepository<num> fakeNumRepository)
        { 
            // Arrange
            IDataRepository dataRepository = new DataRepository(fakeSubmissionRespository,fakePreRepository,fakeNumRepository, this.mapper);

            // Act
            var sut = await dataRepository.GetPresentationData(null,ConstStatements.BS,"dfcd");

            // Assert
            Assert.Null(sut);
        }

        [Theory, GenerateDefaultTestData]
        public async Task GetPresentationData_GivenEmptyString_ExecutesReturnNull_Sceanrio1(IESRepository<sub> fakeSubmissionRespository,
                                                                                                        IESRepository<pre> fakePreRepository,
                                                                                                        IESRepository<num> fakeNumRepository)
        { 
            // Arrange
            IDataRepository dataRepository = new DataRepository(fakeSubmissionRespository,fakePreRepository,fakeNumRepository, this.mapper);

            // Act
            var actual = await dataRepository.GetPresentationData("",ConstStatements.BS,"");

            // Assert
            Assert.Null(actual);
        }

        [Theory, GenerateDefaultTestData]
        public async Task GetPresentationData_GivenEmptyString_ExecutesReturnNull_Sceanrio2(IESRepository<sub> fakeSubmissionRespository,
                                                                                                        IESRepository<pre> fakePreRepository,
                                                                                                        IESRepository<num> fakeNumRepository)
        { 
            // Arrange
            IDataRepository dataRepository = new DataRepository(fakeSubmissionRespository,fakePreRepository,fakeNumRepository, this.mapper);

            // Act
            var actual = await dataRepository.GetPresentationData("",ConstStatements.BS,"sdsd");

            // Assert
            Assert.Null(actual);
        }

        [Theory, GenerateDefaultTestData]
        public async Task GetPresentationData_GivenEmptyString_ExecutesReturnNull_Sceanrio3(IESRepository<sub> fakeSubmissionRespository,
                                                                                                        IESRepository<pre> fakePreRepository,
                                                                                                        IESRepository<num> fakeNumRepository)
        { 
            // Arrange
            IDataRepository dataRepository = new DataRepository(fakeSubmissionRespository,fakePreRepository,fakeNumRepository, this.mapper);

            // Act
            var actual = await dataRepository.GetPresentationData("sds",ConstStatements.BS,"");

            // Assert
            Assert.Null(actual);
        }

        [Theory, GenerateDefaultTestData]
        public async Task GetPresentationData_GivenValidInput_ExecutesReturnValidObject(string adsh,
                                                                                                    string version,
                                                                                                        ConstStatements statements,
                                                                                                        [Frozen] Mock<IESRepository<pre>> mockESPreRepo,
                                                                                                        IESRepository<sub> fakeSubRepository,
                                                                                                        IESRepository<num> fakeNumRepository,
                                                                                                        BaseEntity<pre> sample)
        { 
            // Arrange
            
            mockESPreRepo.Setup(_=>_.FindWithMultipleFilters(It.IsAny<IDictionary<string,string>>())).ReturnsAsync(sample);
            IDataRepository dataRepository = new DataRepository(fakeSubRepository,mockESPreRepo.Object,fakeNumRepository,this.mapper);

            // Act
            var sut = await dataRepository.GetPresentationData(adsh,statements,version);

            // Assert
            Assert.NotNull(sut);
            Assert.Equal(sut.ToList().Count(),sample.Results.ToList().Count());
            Assert.IsType<PreDto>(sut.ToList().First());            
        }
    }
}