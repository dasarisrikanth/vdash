using System;
using System.Threading.Tasks;
using AutoFixture.Xunit2;
using AutoMapper;
using Moq;
using vd.web.Entittes;
using vd.web.ESLayer;
using vd.web.ESLayer.ESEntities;
using vd.web.MappingProfiles;
using vd.web.Services;
using vd.web.tests.Helpers;
using Xunit;

namespace vd.web.tests.Services
{
    public class DataRespository_IsBusinessExists_Tests
    {
        private readonly IMapper mapper;
        public DataRespository_IsBusinessExists_Tests()
        {
            var config = new MapperConfiguration(cfg=>{
                cfg.AddProfile(new SubMappingProfile());
            });

            mapper = config.CreateMapper();
        }

        [Theory, GenerateDefaultTestData]
        public async Task IsBusinessExists_GivenValidInput_ExecutesWithOutException(string adsh, 
                                                                                                        IESRepository<sub> fakeSubmissionRespository,
                                                                                                        IESRepository<pre> fakePreRepository,
                                                                                                        IESRepository<num> fakeNumRepository)
        {
            // Arrange
            IDataRepository dataRepository = new DataRepository(fakeSubmissionRespository,fakePreRepository,fakeNumRepository, this.mapper);

            // Act
            Exception shouldBeNull = await Record.ExceptionAsync(()=>dataRepository.IsBusinessExists(adsh));

            // Assert
            Assert.Null(shouldBeNull);
        }


        [Theory, GenerateDefaultTestData]
        public async Task IsBusinessExists_GivenNullInput_ExecutesReturnNull(IESRepository<sub> fakeSubmissionRespository,
                                                                                                        IESRepository<pre> fakePreRepository,
                                                                                                        IESRepository<num> fakeNumRepository)
        { 
            // Arrange
            IDataRepository dataRepository = new DataRepository(fakeSubmissionRespository,fakePreRepository,fakeNumRepository, this.mapper);

            // Act
            var sut = await dataRepository.IsBusinessExists(null);

            // Assert
            Assert.False(sut);
        }

        [Theory, GenerateDefaultTestData]
        public async Task IsBusinessExists_GivenEmptyString_ExecutesReturnNull(IESRepository<sub> fakeSubmissionRespository,
                                                                                                        IESRepository<pre> fakePreRepository,
                                                                                                        IESRepository<num> fakeNumRepository)
        { 
            // Arrange
            IDataRepository dataRepository = new DataRepository(fakeSubmissionRespository,fakePreRepository,fakeNumRepository, this.mapper);

            // Act
            var actual = await dataRepository.IsBusinessExists("");

            // Assert
            Assert.False(actual);
        }

        [Theory, GenerateDefaultTestData]
        public async Task IsBusinessExists_GivenValidInput_ExecutesReturnValidObject(string adsh,[Frozen] Mock<IESRepository<sub>> mockESSubRepo,
                                                                                                        IESRepository<pre> fakePreRepository,
                                                                                                        IESRepository<num> fakeNumRepository,
                                                                                                        sub sampleSub)
        { 
            // Arrange
            
            mockESSubRepo.Setup(_=>_.FindById(It.IsAny<string>())).ReturnsAsync(sampleSub);
            IDataRepository dataRepository = new DataRepository(mockESSubRepo.Object,fakePreRepository,fakeNumRepository,this.mapper);

            // Act
            var sut = await dataRepository.IsBusinessExists(adsh);

            // Assert
            Assert.NotNull(sut);
            Assert.IsType<Boolean>(sut);
        }
    }
}