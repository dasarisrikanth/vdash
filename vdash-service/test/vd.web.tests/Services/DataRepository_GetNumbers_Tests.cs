using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoFixture.Xunit2;
using AutoMapper;
using Moq;
using vd.web.Entittes;
using vd.web.ESLayer;
using vd.web.ESLayer.ESEntities;
using vd.web.MappingProfiles;
using vd.web.Services;
using vd.web.tests.Helpers;
using Xunit;

namespace vd.web.tests.Services
{
    public class DataRepository_GetNumbers_Tests
    {
        private readonly IMapper mapper;
        public DataRepository_GetNumbers_Tests()
        {
            var config = new MapperConfiguration(cfg=>{
                cfg.AddProfile(new NumMappingProfile());
            });

            mapper = config.CreateMapper();
        }

        [Theory, GenerateDefaultTestData]
        public async Task GetNumbers_GivenValidInput_ExecutesWithOutException(string adsh,
                                                                                string version,
                                                                                string ddate,
                                                                                IESRepository<sub> fakeSubmissionRespository,
                                                                                IESRepository<pre> fakePreRepository,
                                                                                IESRepository<num> fakeNumRepository)
        {
            // Arrange
            IDataRepository dataRepository = new DataRepository(fakeSubmissionRespository,fakePreRepository,fakeNumRepository, this.mapper);

            // Act
            Exception shouldBeNull = await Record.ExceptionAsync(()=>dataRepository.GetNumbers(adsh,version,ddate));

            // Assert
            Assert.Null(shouldBeNull);
        }

        [Theory, GenerateDefaultTestData]
        public async Task GetNumbers_GivenNullInput_ExecutesReturnNull_Scenario1(IESRepository<sub> fakeSubmissionRespository,
                                                                                                        IESRepository<pre> fakePreRepository,
                                                                                                        IESRepository<num> fakeNumRepository)
        { 
            // Arrange
            IDataRepository dataRepository = new DataRepository(fakeSubmissionRespository,fakePreRepository,fakeNumRepository, this.mapper);

            // Act
            var actual = await dataRepository.GetNumbers(null,null,null);

            // Assert
            Assert.Null(actual);
        }

        [Theory, GenerateDefaultTestData]
        public async Task GetNumbers_GivenNullInput_ExecutesReturnNull_Scenario2(IESRepository<sub> fakeSubmissionRespository,
                                                                                                        IESRepository<pre> fakePreRepository,
                                                                                                        IESRepository<num> fakeNumRepository)
        { 
            // Arrange
            IDataRepository dataRepository = new DataRepository(fakeSubmissionRespository,fakePreRepository,fakeNumRepository, this.mapper);

            // Act
            var actual = await dataRepository.GetNumbers(null,"sdsd","sdsd");

            // Assert
            Assert.Null(actual);
        }

        [Theory, GenerateDefaultTestData]
        public async Task GetNumbers_GivenNullInput_ExecutesReturnNull_Scenario3(IESRepository<sub> fakeSubmissionRespository,
                                                                                                        IESRepository<pre> fakePreRepository,
                                                                                                        IESRepository<num> fakeNumRepository)
        { 
            // Arrange
            IDataRepository dataRepository = new DataRepository(fakeSubmissionRespository,fakePreRepository,fakeNumRepository, this.mapper);

            // Act
            var actual = await dataRepository.GetNumbers("sdsd",null,"sdsd");

            // Assert
            Assert.Null(actual);
        }

        [Theory, GenerateDefaultTestData]
        public async Task GetNumbers_GivenNullInput_ExecutesReturnNull_Scenario4(IESRepository<sub> fakeSubmissionRespository,
                                                                                                        IESRepository<pre> fakePreRepository,
                                                                                                        IESRepository<num> fakeNumRepository)
        { 
            // Arrange
            IDataRepository dataRepository = new DataRepository(fakeSubmissionRespository,fakePreRepository,fakeNumRepository, this.mapper);

            // Act
            var actual = await dataRepository.GetNumbers("sdsd","jhdj",null);

            // Assert
            Assert.Null(actual);
        }

        [Theory, GenerateDefaultTestData]
        public async Task GetNumbers_GivenEmptyString_ExecutesReturnNull_Scenario1(IESRepository<sub> fakeSubmissionRespository,
                                                                                                        IESRepository<pre> fakePreRepository,
                                                                                                        IESRepository<num> fakeNumRepository)
        { 
            // Arrange
            IDataRepository dataRepository = new DataRepository(fakeSubmissionRespository,fakePreRepository,fakeNumRepository, this.mapper);

            // Act
            var actual = await dataRepository.GetNumbers("","","");

            // Assert
            Assert.Null(actual);
        }

        [Theory, GenerateDefaultTestData]
        public async Task GetNumbers_GivenEmptyString_ExecutesReturnNull_Scenario2(IESRepository<sub> fakeSubmissionRespository,
                                                                                                        IESRepository<pre> fakePreRepository,
                                                                                                        IESRepository<num> fakeNumRepository)
        { 
            // Arrange
            IDataRepository dataRepository = new DataRepository(fakeSubmissionRespository,fakePreRepository,fakeNumRepository, this.mapper);

            // Act
            var actual = await dataRepository.GetNumbers("","s","s");

            // Assert
            Assert.Null(actual);
        }

        [Theory, GenerateDefaultTestData]
        public async Task GetNumbers_GivenEmptyString_ExecutesReturnNull_Scenario3(IESRepository<sub> fakeSubmissionRespository,
                                                                                                        IESRepository<pre> fakePreRepository,
                                                                                                        IESRepository<num> fakeNumRepository)
        { 
            // Arrange
            IDataRepository dataRepository = new DataRepository(fakeSubmissionRespository,fakePreRepository,fakeNumRepository, this.mapper);

            // Act
            var actual = await dataRepository.GetNumbers("s","","s");

            // Assert
            Assert.Null(actual);
        }

        [Theory, GenerateDefaultTestData]
        public async Task GetNumbers_GivenEmptyString_ExecutesReturnNull_Scenario4(IESRepository<sub> fakeSubmissionRespository,
                                                                                                        IESRepository<pre> fakePreRepository,
                                                                                                        IESRepository<num> fakeNumRepository)
        { 
            // Arrange
            IDataRepository dataRepository = new DataRepository(fakeSubmissionRespository,fakePreRepository,fakeNumRepository, this.mapper);

            // Act
            var actual = await dataRepository.GetNumbers("s","s","");

            // Assert
            Assert.Null(actual);
        }

        [Theory, GenerateDefaultTestData]
        public async Task GetBusinessBySearch_GivenValidInput_ExecutesReturnValidObject(string adsh,
                                                                                        string version,
                                                                                        string ddate,
                                                                                        [Frozen] Mock<IESRepository<num>> mockESNumRepo,
                                                                                        IESRepository<pre> fakePreRepository,
                                                                                        IESRepository<sub> fakeSubRepository,
                                                                                        BaseEntity<num> sampleSub)
        { 
            // Arrange
            
            mockESNumRepo.Setup(_=>_.FindWithMultipleFilters(It.IsAny<IDictionary<string,string>>())).ReturnsAsync(sampleSub);
            IDataRepository dataRepository = new DataRepository(fakeSubRepository,fakePreRepository,mockESNumRepo.Object,this.mapper);

            // Act
            var sut = await dataRepository.GetNumbers(adsh,version,ddate);

            // Assert
            Assert.NotNull(sut);
            Assert.Equal(sut.ToList().Count(),sampleSub.Results.ToList().Count());
            Assert.IsType<NumDto>(sut.ToList().First());
        }
    }
}