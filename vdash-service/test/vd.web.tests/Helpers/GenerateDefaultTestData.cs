using AutoFixture;
using AutoFixture.AutoMoq;
using AutoFixture.Xunit2;

namespace vd.web.tests.Helpers
{
    public class GenerateDefaultTestData:AutoDataAttribute
    {
        public GenerateDefaultTestData():base(GetDefaultFixture)
        {
            
        }

        public static IFixture GetDefaultFixture()
        {
            var autoMoqCustomization = new AutoMoqCustomization();

            var fixture = new Fixture().Customize(autoMoqCustomization);
            fixture.Customizations.Add(new PropertyCustomizations());

            return fixture;
        }
    }
}