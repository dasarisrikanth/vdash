using System;
using System.Reflection;
using AutoFixture.Kernel;
using extensionlib;

namespace vd.web.tests.Helpers
{
    public class PropertyCustomizations:ISpecimenBuilder
    {
        public object Create(object request, ISpecimenContext context)
        {
            var propertyInfo = request as PropertyInfo;
            Random random = new Random();

            if(propertyInfo.IsNotNull())
            {
                if(propertyInfo.Name == "wksi" && propertyInfo.PropertyType == typeof(string))
                {
                    return random.Next(0,2).ToString();
                }
                if(propertyInfo.Name == "prevrpt" && propertyInfo.PropertyType == typeof(string))
                {
                    return random.Next(0,2).ToString();
                }
                if(propertyInfo.Name == "detail" && propertyInfo.PropertyType == typeof(string))
                {
                    return random.Next(0,2).ToString();
                }
                if(propertyInfo.Name == "qtrs" && propertyInfo.PropertyType == typeof(string))
                {
                    return random.Next(0,2).ToString();
                }
                if(propertyInfo.Name == "value" && propertyInfo.PropertyType == typeof(string))
                {
                    return random.Next(0,99999999).ToString();
                }
                if(propertyInfo.Name == "period" && propertyInfo.PropertyType == typeof(string))
                {
                    return DateTime.Today.ToString("yyyyMMdd");
                }
                if(propertyInfo.Name == "filed" && propertyInfo.PropertyType == typeof(string))
                {
                    return DateTime.Today.ToString("yyyyMMdd");
                }
                if(propertyInfo.Name == "accepted" && propertyInfo.PropertyType == typeof(string))
                {
                    return DateTime.Today.ToString("yyyy-MM-dd HH:mm:ss.f");
                }
                if(propertyInfo.Name=="ddate" && propertyInfo.PropertyType == typeof(string))
                {
                    return DateTime.Today.ToString("yyyyMMdd");
                }
            }

            return new NoSpecimen();
            
        }
    }
}